package com.senac.ads.experts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ListaConvidadosAdsCentroApplication {

	public static void main(String[] args) {
		SpringApplication.run(ListaConvidadosAdsCentroApplication.class, args);
	}

}
